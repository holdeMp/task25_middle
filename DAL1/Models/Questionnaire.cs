﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace Task23_Advanced.Models
{
    /// <summary>
    /// Questionnaire Model to represent questionnaires.
    /// </summary>
    [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
    public class Questionnaire
    {
        [Required]
        public int Id { get; set; }
        [Remote("IsFullNameCorrect", "Questionnaire", ErrorMessage = "Incorrect full name")]
        [Required]
        [StringLength(100)]
        public string FullName { get; set; }
        public string Adress { get; set; }
        public bool? Scales { get; set; }
        public bool? Horns { get; set; }
        public bool? City { get; set; }
        public bool? Work { get; set; }
        [Remote("IsEmailAvailable", "Questionnaire",ErrorMessage ="Incorrect email")]
        public string Email { get; set; }
        public string Organisation { get; set; }
        public string Region { get; set; }
        public string Country { get; set; }
        public string Experience { get; set; }
        public string Position { get; set; }
    }
}