﻿using DAL1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL1.Interfaces
{
    public interface IArticleRepository<T> where T : class
    {
        IEnumerable<T> GetAll();
        IEnumerable<Tag> GetAllTags();
        string AddTag(int ArticleId, string TagName);
    }
}
