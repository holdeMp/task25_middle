﻿using DAL1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task23_Advanced.Models;

namespace DAL1.Interfaces
{
     public interface IUnitOfWork : IDisposable
    {   
        IArticleRepository<Article> Articles { get;}
        IRepository<Review> Reviews { get;}
        IRepository<News> News { get; }
        void Save();
    }
}
