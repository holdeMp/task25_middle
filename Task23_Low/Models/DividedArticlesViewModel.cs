﻿using DAL1.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Task23_Advanced.Models
{
    public class DividedArticlesViewModel
    {
        public int Id { get; set; }
        public string AuthorName { get; set; }
        [DisplayFormat(DataFormatString = "{0:dddd, dd MMMM yyyy}")]
        public DateTime Date { get; set; }
        public string LessContent { get; set; }
        public string MoreContent { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
    }
}