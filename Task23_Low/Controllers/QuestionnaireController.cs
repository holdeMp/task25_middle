﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text.RegularExpressions;
namespace Task23_Advanced.Controllers
{
    public class QuestionnaireController : Controller
    {
       
        public ActionResult Index()
        {
            ViewBag.Scales = false;
            ViewBag.Horns = false;
            ViewBag.City = false;
            ViewBag.Works = false;
            
            return View();
        }
        public JsonResult IsFullNameCorrect(string FullName)
        {
            Regex pattern =new Regex( @"[a-zA-Z'-'\s]*");
            if (pattern.IsMatch(FullName))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }
       [HttpGet]
        public JsonResult IsEmailAvailable(string Email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(Email);
                if (addr.Address == Email)
                    return Json(true,JsonRequestBehavior.AllowGet);
                else
                    return Json(false, JsonRequestBehavior.AllowGet);
            }
            catch
            {

                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult UserPassedData(FormCollection form)
        {

            ViewBag.TextReturn = "Your input fullname: " + form["Text"];
            ViewBag.AdressReturn = "Your input adress: " + form["Adress"];
            ViewBag.ScalesReturn = "You do not have scales";
            ViewBag.HornsReturn = "You do not have horns";
            ViewBag.RegionReturn = "Your input region: " + form["Region"];
            ViewBag.OrganisationReturn = "Your input organisation: " + form["Organisation"];
            ViewBag.EmailReturn = "Your input email: " + form["Email"];
            if (Convert.ToBoolean(form["Check-box-scales"].Split(',')[0]))
            {
                ViewBag.ScalesReturn = "You have scales";
            }
            if (Convert.ToBoolean(form["Check-box-horns"].Split(',')[0]))
            {
                ViewBag.HornsReturn = "You have horns";
            }
            ViewBag.CityReturn = "You did not checked city";
            if (form["Check-box-city"] != null && Convert.ToBoolean(form["Check-box-city"].Split(',')[0]))
            {
                ViewBag.CityReturn = "You checked city";
            }
            ViewBag.WorkReturn = "You did not checked work";
            if (form["Check-box-work"] != null && Convert.ToBoolean(form["Check-box-work"].Split(',')[0]))
            {
                ViewBag.WorkReturn = "You checked work";
            }
            ViewBag.CountryReturn = "Your input country: " + form["Country"];
            ViewBag.ExperienceReturn = "Your input experience : " + form["Experience"];
            ViewBag.PositionReturn = "Your input position: " + form["Position"];
            return View();
        }
    }
}