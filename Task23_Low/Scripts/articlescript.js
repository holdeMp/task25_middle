﻿$("#Tags").change(function () {
    if ($(this).val() !== "") {
        $("#TagName").prop('disabled', true);
    }
    else {
        $("#TagName").prop('disabled', false);
    }
});